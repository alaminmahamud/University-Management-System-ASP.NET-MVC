# University-Management-System-ASP.NET-MVC
A simple university management system using ASP.NET Mvc with Entity Network Code First

### Technologies
- ADO.NET
- Entity Framework
- ASP.NET
- MVC
 
### Topics
* Asynchronus Programming
* Entity Framework
* Concurrency
* CodeFirst
* Inheritence
* Lazy Loading
* Data Annotations
* Code First Migrations
* Eager Coding
* Connection Resiliency

### Actions Have Been Done On the Site

* Creating a data model using data annotations attributes an fluent API for database mapping
* Performing basic CRUD Operations Filtering, Ordering, Grouping
<hr/>


