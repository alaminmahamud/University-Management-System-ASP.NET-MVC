﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CUET_Management_System.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CUET_Management_System.DAL
{
    public class SchoolContext : DbContext
    {
        // why constructor is being called i don't know
        
        /// <summary>
        /// Specifying the connection string
        /// if you don't specify a connection string or the name of one
        /// that connectString name is the same as the class name
        /// </summary>


        public SchoolContext() : base("SchoolContext")
        {
        
        }
        
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }

        // OnModelCreating ?

            /// <summary>
            /// Specifying Singular Table Name
            /// </summary>
            /// <param name="modelBuilder"></param>


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}